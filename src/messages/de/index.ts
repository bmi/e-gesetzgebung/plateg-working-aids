// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const de = {
  workingAids: {
    generalErrorMsg:
      'Es ist ein unerwarteter Fehler aufgetreten. Bitte wenden Sie sich an den Support. Wir entschuldigen uns für entstandene Umstände. {{fehlercode}}',
    home: {
      mainTitle: 'Arbeitshilfenbibliothek',
      search: {
        placeholder: 'Suchbegriff eingeben',
        btnText: 'Suchen',
        hintLabel: 'Hinweis: ',
        hintContentPart1: 'Eine Arbeitshilfe ist nicht auf aktuellem Stand? Gerne können Sie uns per ',
        hintContentPart2: ' informieren.',
        hintCollapseLabel: 'Hinweis zur Zugänglichkeit',
        hintCollapseContent: `<p class="p-nostyle">Wir bemühen uns, sämtliche Dokumente in der Arbeitshilfenbibliothek barrierefrei zur Verfügung zu stellen. Sobald barrierefreie Versionen der betroffenen Arbeitshilfen vorliegen, werden diese in der Arbeitshilfenbibliothek aktualisiert.</p>
                              Folgende Arbeitshilfen sind zum aktuellen Zeitpunkt barrierefrei zugänglich:
                              <ul class="ul-nostyle">
                                <li>AH GFA: Analyse Regelungsfeld (Word)</li>
                                <li>AH GFA: Zielbeschreibung (Word)</li>
                                <li>AH GFA: Alternativenfindung (Word)</li>
                                <li>AH GFA: Int. Alternativenprüfung (Word)</li>
                                <li>AH GFA: Konsultation (Word)</li>
                                <li>AH GFA: Fragenkatalog (Word)</li>
                                <li>AH Experimentierklauseln (PDF)</li>
                              </ul>`,
        clearText: 'Suche zurücksetzen',
        mail: {
          address: 'support.egesetzgebung@bmi.bund.de',
          subject: 'Arbeitshilfe in der Arbeitshilfenbibliothek der E-Gesetzgebung nicht mehr aktuell',
          content:
            'Sehr geehrte Damen und Herren, \n\ngerne möchte ich Ihnen mitteilen, dass diese Arbeitshilfe in der Arbeitshilfenbibliothek der E-Gesetzgebung nicht auf neustem Stand ist: \n[Link einfügen]  \n\nDie neuste Fassung finden Sie hier bzw. im Anhang.  \n[Ggf. Link einfügen bzw. Text anpassen und Anhang beifügen] \n\nGerne dürfen Sie mich für Rückfragen kontaktieren. \n\nMit freundlichen Grüßen',
          linktText: 'E-Mail',
        },
      },
      publisher: 'Herausgegeben von: ',
      updated: ' · Aktualisierung ',
      pdfLabel: '(PDF)',
      numberOfEntriesLabel: 'Einträge',
      sorter: {
        titleDesc: 'Alphabetisch (Z-A)',
        titleAsc: 'Alphabetisch (A-Z)',
        dateDesc: 'Aktualisierung (neueste zuerst)',
        dateAsc: 'Aktualisierung (älteste zuerst)',
      },
    },
  },
};
