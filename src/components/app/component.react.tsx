// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../styles/plateg-working-aids.less';

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { ArbeitshilfenControllerApi, Configuration } from '@plateg/rest-api';
import { configureRestApi, GlobalDI } from '@plateg/theme';

import { WorkingAidsLayoutWrapper } from '../working-aids/component.react';

export function WorkingAidsApp(): React.ReactElement {
  configureRestApi(registerRestApis);

  return (
    <Switch>
      <Route path="/hilfen">
        <WorkingAidsLayoutWrapper />
      </Route>
    </Switch>
  );
}

function registerRestApis(configRestCalls: Configuration) {
  GlobalDI.getOrRegister('arbeitshilfenController', () => new ArbeitshilfenControllerApi(configRestCalls));
}
