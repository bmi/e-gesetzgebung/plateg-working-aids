// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './home.less';

import { Col, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import { AjaxError } from 'rxjs/ajax';

import { WorkHelpEntry } from '@plateg/rest-api';
import { ErrorController, GlobalDI, WorkingAidsController } from '@plateg/theme';

import { WorkingAidsContentSectionComponent } from './content-section/component.react';
import { WorkingAidsHeadSectionComponent } from './head-section/component.react';
export function WorkingAidsHomeComponent(): React.ReactElement {
  const ctrl = GlobalDI.getOrRegister('workingAidsController', () => new WorkingAidsController());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [workingAids, setWorkingAids] = useState<WorkHelpEntry[]>([]);
  const [filteredWorkingAids, setFilteredWorkingAids] = useState<WorkHelpEntry[]>([]);

  useEffect(() => {
    ctrl.getArbeitshilfenListCall().subscribe({
      next: (workHelpEntries: WorkHelpEntry[]) => {
        setWorkingAids(workHelpEntries);
        setFilteredWorkingAids(workHelpEntries);
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'workingAids.generalErrorMsg');
      },
    });
  }, []);

  const onSearch = (value: string) => {
    setFilteredWorkingAids(workingAids.filter((item) => ctrl.searchEntry(value, item)));
  };

  return (
    <div className="working-aids-home-page">
      <Row className="head-section-row">
        <Col span={24}>
          <div className="holder">
            <div className="head-section-holder">
              <WorkingAidsHeadSectionComponent onSearch={onSearch} />
            </div>
          </div>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <div className="holder">
            <div className="main-content-holder">
              <WorkingAidsContentSectionComponent workingAids={filteredWorkingAids} />
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
}
