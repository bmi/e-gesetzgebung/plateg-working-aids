// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './head-section.less';

import { Collapse } from 'antd';
import Search from 'antd/lib/input/Search';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { DirectionRightOutlinedNew } from '@plateg/theme/src/components/icons';

export function WorkingAidsHeadSectionComponent(props: { onSearch: (value: string) => void }): React.ReactElement {
  const { t } = useTranslation();
  return (
    <div className="head-section emails-search-holder">
      <Title level={1}>{t('workingAids.home.mainTitle')}</Title>
      <Search
        id="workingAids-searchField-searchBox"
        className="search-field"
        placeholder={t('workingAids.home.search.placeholder')}
        size="large"
        enterButton={t('workingAids.home.search.btnText')}
        onSearch={props.onSearch}
        allowClear={true}
      />
      <dl>
        <dt>{t('workingAids.home.search.hintLabel')}</dt>
        <dd>
          {t('workingAids.home.search.hintContentPart1')}
          <a
            id="workingAids-aktualitaetEmail-link"
            href={`mailto:${t('workingAids.home.search.mail.address')}?subject=${encodeURI(
              t('workingAids.home.search.mail.subject'),
            )}&body=${encodeURI(t('workingAids.home.search.mail.content'))}`}
          >
            {t('workingAids.home.search.mail.linktText')}
          </a>
          {t('workingAids.home.search.hintContentPart2')}
        </dd>
        <Collapse
          ghost
          items={[
            {
              label: <dt>{t('workingAids.home.search.hintCollapseLabel')}</dt>,
              children: (
                <dd
                  dangerouslySetInnerHTML={{
                    __html: t('workingAids.home.search.hintCollapseContent', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              ),
            },
          ]}
          expandIcon={({ isActive }) => (
            <DirectionRightOutlinedNew
              style={{ width: 29, height: 29 }}
              className={isActive ? 'svg-transition svg-rotate-down' : 'svg-transition'}
            />
          )}
        />
      </dl>
    </div>
  );
}
