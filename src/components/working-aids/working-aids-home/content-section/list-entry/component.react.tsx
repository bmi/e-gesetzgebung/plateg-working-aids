// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './list-entry.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { WorkHelpEntryFormatEnum } from '@plateg/rest-api';
import { TwoLineText } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

export interface ListEntryProps {
  link: string;
  firstRow: string;
  secondRow: string;
  publishers: string[];
  updated?: string;
  index: string;
  format: WorkHelpEntryFormatEnum;
}

export function ListEntryComponent(props: ListEntryProps): React.ReactElement {
  const { t } = useTranslation();
  const [thirdRow, setThirdRow] = useState<string>('');

  useEffect(() => {
    let publishers = '';
    props.publishers.forEach((publisher, index) => {
      let name;
      if (index === 0) {
        name = t('workingAids.home.publisher') + publisher;
      } else {
        name = '/' + publisher;
      }
      publishers += name;
    });
    const updated = props.updated ? t('workingAids.home.updated') + Filters.dateFromString(props.updated) : '';
    setThirdRow(publishers + updated);
  }, [props]);

  const prepareDocumentType = (format: WorkHelpEntryFormatEnum) => {
    let formatLabel = '';
    switch (format) {
      case WorkHelpEntryFormatEnum.Pdf:
        formatLabel = `(${format})`;
        break;
      case WorkHelpEntryFormatEnum.Docx:
        formatLabel = `(Word)`;
        break;
      case WorkHelpEntryFormatEnum.Xlsm:
        formatLabel = `(Excel)`;
        break;
    }

    return formatLabel;
  };

  return (
    <li className="list-entry">
      <div className="list-entry-link-container">
        <a
          id={`workingAids-listEntry-link-${props.index}`}
          target="#"
          href={props.link}
          className={props.format !== WorkHelpEntryFormatEnum.Html ? 'internal-link' : ''}
        >
          {props.firstRow} {prepareDocumentType(props.format)}
        </a>
      </div>
      <TwoLineText
        firstRow={props.secondRow}
        firstRowBold={true}
        secondRow={thirdRow}
        secondRowBold={false}
        secondRowLight={true}
      />
      <div className="vertical-seperator" />
    </li>
  );
}
