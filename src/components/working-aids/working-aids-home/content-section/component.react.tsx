// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './content-section.less';

import { Col, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { WorkHelpEntry } from '@plateg/rest-api';
import { GlobalDI, SortedInfo, SorterComponent } from '@plateg/theme';

import { ContentSectionController } from './controller';
import { ListEntryComponent, ListEntryProps } from './list-entry/component.react';
interface WorkingAidsContentSectionComponentProps {
  workingAids: WorkHelpEntry[];
}
export function WorkingAidsContentSectionComponent(props: WorkingAidsContentSectionComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const ctrl = GlobalDI.getOrRegister('ContentSectionController', () => new ContentSectionController());
  const [listEntries, setListEntries] = useState<ListEntryProps[]>([]);
  const [sortedInfo, setSortedInfo] = useState<SortedInfo>();
  const [sorterOptions] = useState([
    {
      columnKey: 'title',
      titleAsc: t('workingAids.home.sorter.titleAsc'),
      titleDesc: t('workingAids.home.sorter.titleDesc'),
    },
    {
      columnKey: 'publicationDate',
      titleAsc: t('workingAids.home.sorter.dateAsc'),
      titleDesc: t('workingAids.home.sorter.dateDesc'),
    },
  ]);

  useEffect(() => {
    if (sortedInfo) {
      setListEntries(ctrl.createWorkingAidsList(ctrl.sortEntries(sortedInfo, [...props.workingAids])));
    } else {
      setListEntries(ctrl.createWorkingAidsList([...props.workingAids]));
    }
  }, [sortedInfo, props.workingAids]);

  return (
    <div className="content-section">
      <Row>
        <Col className="list-head">
          <h2>
            {listEntries.length} {t('workingAids.home.numberOfEntriesLabel')}
          </h2>
          <div className="filter-row">
            <SorterComponent
              sorterOptions={sorterOptions}
              setSortedInfo={setSortedInfo}
              sortedInfo={sortedInfo}
              customDefaultSortIndex={1}
            />
          </div>
        </Col>
      </Row>

      <div className="vertical-seperator" />
      <ul>
        {listEntries.map((entry, index) => {
          return (
            <ListEntryComponent
              key={`${entry.firstRow}-${index.toString()}`}
              firstRow={entry.firstRow}
              link={entry.link}
              secondRow={entry.secondRow}
              publishers={entry.publishers}
              updated={entry.updated}
              index={index.toString()}
              format={entry.format}
            />
          );
        })}
      </ul>
    </div>
  );
}
