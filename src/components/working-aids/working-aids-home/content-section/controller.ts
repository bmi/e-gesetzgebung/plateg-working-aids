// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { WorkHelpEntry } from '@plateg/rest-api/models';
import { compareDates, SortedInfo } from '@plateg/theme';

import { ListEntryProps } from './list-entry/component.react';
export class ContentSectionController {
  public createWorkingAidsList(entries: WorkHelpEntry[]): ListEntryProps[] {
    return entries.map((entry) => {
      return {
        link: entry.url,
        firstRow: entry.title,
        secondRow: entry.description,
        publishers: entry.publishers,
        updated: entry.publicationDate,
        format: entry.format,
      };
    });
  }

  public sortEntries(sortedInfo: SortedInfo, workingAids: WorkHelpEntry[]): WorkHelpEntry[] {
    if (sortedInfo.columnKey === 'title') {
      if (sortedInfo.order === 'ascend') {
        workingAids.sort((o1, o2) => o1.title.localeCompare(o2.title));
      }
      if (sortedInfo.order === 'descend') {
        workingAids.sort((o1, o2) => o2.title.localeCompare(o1.title));
      }
    }
    if (sortedInfo.columnKey === 'publicationDate') {
      if (sortedInfo.order === 'ascend') {
        workingAids.sort((o1, o2) => compareDates(o1.publicationDate, o2.publicationDate));
      }
      if (sortedInfo.order === 'descend') {
        workingAids.sort((o1, o2) => compareDates(o2.publicationDate, o1.publicationDate));
      }
    }
    return workingAids;
  }
}
