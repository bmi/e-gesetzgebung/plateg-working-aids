// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect, use } from 'chai';
import * as chaiArrays from 'chai-arrays';

import { WorkHelpEntry, WorkHelpEntryFormatEnum } from '@plateg/rest-api/models';
import { SortedInfo } from '@plateg/theme';

import { ContentSectionController } from './controller';
use(chaiArrays.default);

const ctrl = new ContentSectionController();
const entries: WorkHelpEntry[] = [
  {
    title: 'Alpha Arbeitshilfe',
    description: 'Dies ist die Arbeitshilfe1',
    format: WorkHelpEntryFormatEnum.Pdf,
    url: 'http://www.exampleWorkEntry.de/Arbeitshilfe1.pdf',
    publishers: ['ATester'],
    publicationDate: '2021-04-07 07:10:30',
  },
  {
    title: 'Beta Arbeitshilfe',
    description: 'Dies ist die Arbeitshilfe2',
    format: WorkHelpEntryFormatEnum.Pdf,
    url: 'http://www.exampleWorkEntry.de/Arbeitshilfe2.pdf',
    publishers: ['ATester2'],
    publicationDate: '2021-05-07 07:10:30',
  },
  {
    title: 'Gamma Arbeitshilfe',
    description: 'Dies ist die Arbeitshilfe3',
    format: WorkHelpEntryFormatEnum.Pdf,
    url: 'http://www.exampleWorkEntry.de/Arbeitshilfe3.pdf',
    publishers: ['ATester3'],
    publicationDate: '2021-06-07 07:10:30',
  },
];
describe('Test creation of ListEntryProps[] from WorkHelpEntry[]', () => {
  it('mapping is correct', () => {
    const listEntryProps = ctrl.createWorkingAidsList(entries);
    entries.forEach((entry, index) => {
      expect(entry.url).to.eql(listEntryProps[index].link);
      expect(entry.title).to.eql(listEntryProps[index].firstRow);
      expect(entry.description).to.eql(listEntryProps[index].secondRow);
      expect(entry.publishers).to.eql(listEntryProps[index].publishers);
      expect(entry.publicationDate).to.eql(listEntryProps[index].updated);
    });
  });
});

describe('Test sorting of WorkhelpEntry[]', () => {
  it('sort by title asc', () => {
    const sortedEntriesTitleAsc = [...entries];
    ctrl.sortEntries(new SortedInfo('ascend', 'title'), sortedEntriesTitleAsc);
    expect(sortedEntriesTitleAsc).to.eql(entries);
  });
  it('sort by title desc', () => {
    const sortedEntriesTitleDesc = [...entries];
    ctrl.sortEntries(new SortedInfo('descend', 'title'), sortedEntriesTitleDesc);
    expect(sortedEntriesTitleDesc[0]).to.eql(entries[2]);
    expect(sortedEntriesTitleDesc[1]).to.eql(entries[1]);
    expect(sortedEntriesTitleDesc[2]).to.eql(entries[0]);
  });
  it('sort by date asc', () => {
    const sortedEntriesDateAsc = [...entries];
    ctrl.sortEntries(new SortedInfo('ascend', 'publicationDate'), sortedEntriesDateAsc);
    expect(sortedEntriesDateAsc).to.eql(entries);
  });
  it('sort by date desc', () => {
    const sortedEntriesDateDesc = [...entries];
    ctrl.sortEntries(new SortedInfo('descend', 'publicationDate'), sortedEntriesDateDesc);
    expect(sortedEntriesDateDesc[0]).to.eql(entries[2]);
    expect(sortedEntriesDateDesc[1]).to.eql(entries[1]);
    expect(sortedEntriesDateDesc[2]).to.eql(entries[0]);
  });
  it('sort faulty key', () => {
    const sortedEntriesFaultyKey = [...entries];
    ctrl.sortEntries(new SortedInfo('descend', 'blub'), sortedEntriesFaultyKey);
    expect(sortedEntriesFaultyKey).to.eql(entries);
  });
});
