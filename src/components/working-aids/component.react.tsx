// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Layout } from 'antd';
import { Content } from 'antd/es/layout/layout';
import React from 'react';

import { WorkingAidsHomeComponent } from './working-aids-home/component.react';

export function WorkingAidsLayoutWrapper(): React.ReactElement {
  return (
    <Layout style={{ height: '100%' }} className="site-layout-background">
      <Content className="main-content-area has-drawer">
        <div className="ant-layout-content">
          <WorkingAidsHomeComponent />
        </div>
      </Content>
    </Layout>
  );
}
